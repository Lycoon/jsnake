package fr.lycoon.jsnake.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import fr.lycoon.jsnake.SnakeGame;

public class DesktopLauncher 
{
	public static void main (String[] arg) 
	{
		SnakeGame game = new SnakeGame();
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "JSnake";
		config.addIcon("snake/head.png", Files.FileType.Internal);
		config.width = game.boardX*game.tileSize;
		config.height = game.boardY*game.tileSize;
		config.foregroundFPS = 30;
		
		new LwjglApplication(game, config);
	}
}

package fr.lycoon.jsnake.screens;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;

import fr.lycoon.jsnake.SnakeGame;

public class MainGame implements Screen
{
	Random rnd;
	int[][] board;
	int direction = 3;
	int length = 4;
	
	int prevHeadX = 0; int prevHeadY = 0;
	int headX = 0, headY = 0;
	int lastX = 0, lastY = 0;
	int foodX, foodY;
	
	public enum Collision 
	{
		FOOD,
		BORDER,
		NOTHING
	}
	
	public void spawnFood()
	{
		int rndPosX = 0, rndPosY = 0;
		int content = -1;
		while (content != 0)
		{
			rndPosX = rnd.nextInt(SnakeGame.boardX);
			rndPosY = rnd.nextInt(SnakeGame.boardY);
			content = board[rndPosX][rndPosY];
		}
		
		board[rndPosX][rndPosY] = 1;
		foodX = rndPosX;
		foodY = rndPosY;
	}
	
	public int pressedKey()
	{
		prevHeadX = headX;
		prevHeadY = headY;
		
		if(Gdx.input.isKeyPressed(Keys.UP))
		{
			headY++;
			return 1;
		}
		else if(Gdx.input.isKeyPressed(Keys.DOWN))
		{
			headY--;
			return 2;
		}
		else if(Gdx.input.isKeyPressed(Keys.LEFT))
		{
			headX--;
			return 3;
		}
		else if(Gdx.input.isKeyPressed(Keys.RIGHT))
		{
			headX++;
			return 4;
		}
		else
			return 0;
	}
	
	public void move()
	{
		Collision col = checkCollision();
		System.out.println(col);
		System.out.println("HeadX: " +headX+ ", HeadY: " +headY);
		System.out.println("BoardX: " +SnakeGame.boardX+ ", BoardY: " +SnakeGame.boardY);
		
		if (col.equals(Collision.BORDER))
			Gdx.app.exit();
		else if (col.equals(Collision.FOOD))
		{
			length++;
			spawnFood();
		}
		
		setCell(headX, headY, 3); // Head goes forward
		setCell(prevHeadX, prevHeadY, 2); // Previous head is replaced with body
		setCell(lastX, lastY, 0); // Deleting last body
	}
	
	public Collision checkCollision()
	{
		// Out of bounds
		if (headX >= SnakeGame.boardX || headX < 0 || 
				headY >= SnakeGame.boardY || headY < 0)
			return Collision.BORDER;
		
		// Collision with itself
		if (board[headX][headY] == 2)
			return Collision.BORDER;
		
		// Collision with food
		if (board[headX][headY] == 1)
			return Collision.FOOD;
		
		return Collision.NOTHING;
	}
	
	public void instantiateSnake()
	{
		headX = 10; headY = 5;
		lastX = 7; lastY = 5;
		direction = 0;
		
		setCell(headX, headY, 3); // Head
		setCell(9, 5, 2); // Body
		setCell(8, 5, 2); // Body
		setCell(lastX, lastY, 2); // Body
	}
	
	@Override
	public void show()
	{
		rnd = new Random();
		
		// Filling board with empty cells
		board = new int[SnakeGame.boardX][SnakeGame.boardY];
		for (int x = 0; x < SnakeGame.boardX; x++)
		{
			for (int y = 0; y < SnakeGame.boardY; y++)
			{
				setCell(x, y, 0);
			}
		}
		
		instantiateSnake();
		spawnFood();
		
		new Timer().scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run() 
			{
				System.out.println("TEST LOOP");
				direction = pressedKey();
				
				if (direction != 0)
					move();
			}
		}, 2000, 300);
	}
	
	@Override
	public void render(float delta) 
	{
		
	}

	@Override
	public void resize(int width, int height)
	{
		
	}

	@Override
	public void pause() 
	{
		
	}

	@Override
	public void resume() 
	{
		
	}

	@Override
	public void hide()
	{
		
	}

	@Override
	public void dispose() 
	{
		
	}

	public int getCell(int x, int y)
	{
		return board[x][y];
	}
	
	public void setCell(int x, int y, int content)
	{
		board[x][y] = content;
	}
}

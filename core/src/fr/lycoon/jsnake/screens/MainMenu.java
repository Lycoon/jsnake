package fr.lycoon.jsnake.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import fr.lycoon.jsnake.SnakeGame;

public class MainMenu implements Screen
{
	Game game;
	Stage stage;
	int w, h;
	
	SpriteBatch titleBatch;
	Button playButton, quitButton;
	
	// Fonts
	BitmapFont titleFont, defaultFont;
	FreeTypeFontParameter titleParam, defaultParam;
	FreeTypeFontGenerator titleGenerator, defaultGenerator;
	
	public MainMenu(final Game game)
	{
		this.game = game;
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();
		
		// Initializing buttons
		Skin mainSkin = new Skin(Gdx.files.internal("ui/skin/uiskin.json"));
		playButton = new TextButton("Play", mainSkin, "default");
		playButton.setSize(w/6, h/12);
		playButton.setPosition(w/2 - playButton.getWidth()/2, h/2 - 20);
		quitButton = new TextButton("Quit", mainSkin, "default");
		quitButton.setSize(w/6, h/12);
		quitButton.setPosition(w/2 - quitButton.getWidth()/2, h/2 - 80);
		
        playButton.addListener(new InputListener()
        {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) 
            {
            	SnakeGame.mainGame = new MainGame();
                game.setScreen(SnakeGame.mainGame);
                return true;
            }
        });
        
        quitButton.addListener(new InputListener()
        {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) 
            {
            	Gdx.app.exit();
                return true;
            }
        });

		// Stage management
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		stage.addActor(playButton);
		stage.addActor(quitButton);
		
		// Generating fonts
		titleGenerator = new FreeTypeFontGenerator(Gdx.files.internal("ui/fonts/Title.ttf"));
		titleParam = new FreeTypeFontParameter();
		titleParam.size = 130;
		
		defaultGenerator = new FreeTypeFontGenerator(Gdx.files.internal("ui/fonts/Default.ttf"));
		defaultParam = new FreeTypeFontParameter();
		defaultParam.size = 40;

		defaultFont = defaultGenerator.generateFont(defaultParam);
		titleFont = titleGenerator.generateFont(titleParam);
		defaultFont.setColor(Color.BLACK);
		titleFont.setColor(Color.WHITE);

		titleBatch = new SpriteBatch();
	}

	@Override
	public void show()
	{

	}

	@Override
	public void render(float delta)
	{
	    Gdx.gl.glClearColor(1, 0.78f, 0.07f, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    stage.act();
	    stage.draw();

	    GlyphLayout titleLayout = new GlyphLayout(titleFont, "JSnake");
	    
		titleBatch.begin();
		titleFont.draw(titleBatch, titleLayout, w/2 - titleLayout.width/2, h-100);
		titleBatch.end();
	}

	@Override
	public void resize(int width, int height) 
	{
		
	}

	@Override
	public void pause() 
	{
		
	}

	@Override
	public void resume() 
	{
		
	}

	@Override
	public void hide()
	{
		playButton.clear();
		quitButton.clear();
	}

	@Override
	public void dispose() 
	{
		titleBatch.dispose();
	}
}

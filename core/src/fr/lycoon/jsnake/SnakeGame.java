package fr.lycoon.jsnake;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import fr.lycoon.jsnake.screens.MainGame;
import fr.lycoon.jsnake.screens.MainMenu;

public class SnakeGame extends Game 
{
	public static int tileSize = 40;
	public static int boardX = 25;
	public static int boardY = 15;
	public static MainGame mainGame;
	
	Game game;
	SpriteBatch batch;
	Texture tile, body, head, food;
	
	public Texture getCellTexture(int content)
	{
		switch(content)
		{
			case 1:
				return food;
			case 2:
				return body;
			case 3:
				return head;
			default:
				return tile;
		}
	}
	
	@Override
	public void create ()
	{
		// Open menu first
		this.setScreen(new MainMenu(this));
		
		game = this;
		batch = new SpriteBatch();
		
		tile = new Texture("board/tile.png");
		body = new Texture("snake/body.png");
		head = new Texture("snake/head.png");
		food = new Texture("board/food.png");
	}

	@Override
	public void render ()
	{
		// Rendering board
		batch.begin();
		if (mainGame != null)
		{
			for (int x = 0; x < boardX; x++)
			{
				for (int y = 0; y < boardY; y++)
				{
					int content = mainGame.getCell(x, y);
					batch.draw(getCellTexture(content), x*tileSize, y*tileSize, tileSize, tileSize);
				}
			}
		}
		batch.end();
		
		super.render();
	}
	
	@Override
	public void dispose () 
	{
		batch.dispose();
		
		tile.dispose();
		body.dispose();
		head.dispose();
		food.dispose();
	}
}
